// Generated by view binder compiler. Do not edit!
package cs.mad.flashcards.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.button.MaterialButton;
import cs.mad.flashcards.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ActivityStudySetBinding implements ViewBinding {
  @NonNull
  private final ConstraintLayout rootView;

  @NonNull
  public final MaterialButton correctFlashcardButton;

  @NonNull
  public final TextView correctNum;

  @NonNull
  public final MaterialButton exitFlashcardButton;

  @NonNull
  public final TextView flashcardSetTitle;

  @NonNull
  public final TextView flashcardTerm;

  @NonNull
  public final TextView missedNum;

  @NonNull
  public final MaterialButton missedSetButton;

  @NonNull
  public final MaterialButton skipSetButton;

  @NonNull
  public final FrameLayout topFrame;

  private ActivityStudySetBinding(@NonNull ConstraintLayout rootView,
      @NonNull MaterialButton correctFlashcardButton, @NonNull TextView correctNum,
      @NonNull MaterialButton exitFlashcardButton, @NonNull TextView flashcardSetTitle,
      @NonNull TextView flashcardTerm, @NonNull TextView missedNum,
      @NonNull MaterialButton missedSetButton, @NonNull MaterialButton skipSetButton,
      @NonNull FrameLayout topFrame) {
    this.rootView = rootView;
    this.correctFlashcardButton = correctFlashcardButton;
    this.correctNum = correctNum;
    this.exitFlashcardButton = exitFlashcardButton;
    this.flashcardSetTitle = flashcardSetTitle;
    this.flashcardTerm = flashcardTerm;
    this.missedNum = missedNum;
    this.missedSetButton = missedSetButton;
    this.skipSetButton = skipSetButton;
    this.topFrame = topFrame;
  }

  @Override
  @NonNull
  public ConstraintLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ActivityStudySetBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ActivityStudySetBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.activity_study_set, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ActivityStudySetBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.correct_flashcard_button;
      MaterialButton correctFlashcardButton = rootView.findViewById(id);
      if (correctFlashcardButton == null) {
        break missingId;
      }

      id = R.id.correct_Num;
      TextView correctNum = rootView.findViewById(id);
      if (correctNum == null) {
        break missingId;
      }

      id = R.id.exit_flashcard_button;
      MaterialButton exitFlashcardButton = rootView.findViewById(id);
      if (exitFlashcardButton == null) {
        break missingId;
      }

      id = R.id.flashcard_set_title;
      TextView flashcardSetTitle = rootView.findViewById(id);
      if (flashcardSetTitle == null) {
        break missingId;
      }

      id = R.id.flashcard_term;
      TextView flashcardTerm = rootView.findViewById(id);
      if (flashcardTerm == null) {
        break missingId;
      }

      id = R.id.missed_Num;
      TextView missedNum = rootView.findViewById(id);
      if (missedNum == null) {
        break missingId;
      }

      id = R.id.missed_set_button;
      MaterialButton missedSetButton = rootView.findViewById(id);
      if (missedSetButton == null) {
        break missingId;
      }

      id = R.id.skip_set_button;
      MaterialButton skipSetButton = rootView.findViewById(id);
      if (skipSetButton == null) {
        break missingId;
      }

      id = R.id.top_frame;
      FrameLayout topFrame = rootView.findViewById(id);
      if (topFrame == null) {
        break missingId;
      }

      return new ActivityStudySetBinding((ConstraintLayout) rootView, correctFlashcardButton,
          correctNum, exitFlashcardButton, flashcardSetTitle, flashcardTerm, missedNum,
          missedSetButton, skipSetButton, topFrame);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}

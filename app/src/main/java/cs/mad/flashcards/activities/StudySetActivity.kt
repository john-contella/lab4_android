package cs.mad.flashcards.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.databinding.ActivityStudySetBinding
import cs.mad.flashcards.entities.Flashcard


class StudySetActivity : AppCompatActivity() {
    private lateinit var binding: ActivityStudySetBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStudySetBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val dataSet = mutableListOf(Flashcard.getHardcodedFlashcards())

        var missedNum = 0
        var correctNum = 0
        var i = 0


        binding.flashcardTerm.setOnClickListener {
           Snackbar.make(binding.root, "Definition Displayed!", Snackbar.LENGTH_LONG).show()
       }

        binding.correctFlashcardButton.setOnClickListener{

            correctNum++
            binding.correctNum.text = "Correct: $correctNum"
            //dataSet.removeAt(i)
            i++
        }

        binding.missedSetButton.setOnClickListener{

            missedNum++
            binding.missedNum.text = "Missed: $missedNum"
            //var temp = dataSet[i]
            //dataSet.removeAt(i)
            //dataSet.add(temp)
            i++


        }

        binding.skipSetButton.setOnClickListener{

            //var temp = dataSet[i]
            //dataSet.removeAt(i)
            //dataSet.add(temp)
            i++

        }

        binding.exitFlashcardButton.setOnClickListener{
            finish()
        }
    }
}